import requests

from easy_request.BaseRequest import BaseRequest


class Sonarr(BaseRequest):
    def __init__(self, token, api_url):
        super().__init__(token, api_url)

    def __get_language_profile(self) -> int:
        host = f"{self.api_url}/languageprofile"
        response = requests.get(host, headers=self.headers)

        return response.json()[0]["id"]  # Returns the first profile

    def find_series(self, term):
        series = self.lookup(term=term, endpoint="series")

        return series

    def request_series(self, series):
        add_options = {
            "addOptions": {"searchForMissingEpisodes": True},
            "languageProfileId": self.__get_language_profile(),
        }

        return self.add_new(to_add=series, endpoint="series", extra=add_options)

    def request_season(self):
        pass

    def request_episode(self):
        pass
