from easy_request.BaseRequest import BaseRequest


class Radarr(BaseRequest):

    def __init__(self, token, api_url):
        super().__init__(token, api_url)

    def find_movie(self, term):
        movie = self.lookup(term=term, endpoint="movie")

        return movie

    def request_movie(self, movie):
        add_options = {
            "addOptions": {
                "searchForMovie": True
            }
        }

        return self.add_new(to_add=movie, endpoint="movie", extra=add_options)
