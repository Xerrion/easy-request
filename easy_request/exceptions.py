class MovieAlreadyExists(Exception):
    pass


class SeriesAlreadyExists(Exception):
    pass
