import requests

from easy_request.exceptions import MovieAlreadyExists, SeriesAlreadyExists


class BaseRequest:
    def __init__(self, token, api_url):
        self.token = token
        self.api_url = api_url
        self.headers = {"X-Api-Key": self.token}

    def __get_root_folder(self) -> str:
        host = f"{self.api_url}/rootfolder"
        response = requests.get(host, headers=self.headers)

        return response.json()[0]["path"]

    def __get_quality_profile(self) -> int:
        host = f"{self.api_url}/qualityProfile"
        response = requests.get(host, headers=self.headers)

        return response.json()[0]["id"]

    def lookup(self, term, endpoint) -> dict:
        params = {"term": term}
        host = f"{self.api_url}/{endpoint}/lookup"
        response = requests.get(host, params=params, headers=self.headers)

        return response.json()

    def add_new(self, to_add, endpoint=None, extra=None):
        to_add |= {
            "QualityProfileId": self.__get_quality_profile(),
            "rootFolderPath": self.__get_root_folder(),
            "monitored": True,
        }

        if extra:
            to_add |= extra

        host = f"{self.api_url}/{endpoint}"
        response = requests.post(host, json=to_add, headers=self.headers)

        if response.status_code == 400:
            error_code = response.json()[1]["errorCode"]
            if error_code == "MovieExistsValidator":
                raise MovieAlreadyExists
            elif error_code == "SeriesExistsValidator":
                raise SeriesAlreadyExists
        else:
            return response.json()
