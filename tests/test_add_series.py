import os
import unittest

from easy_request import Radarr, Sonarr
from easy_request.exceptions import SeriesAlreadyExists

radarr = Radarr(
    token=os.getenv("RADARR_TOKEN"), api_url=os.getenv("RADARR_URL")
)
sonarr = Sonarr(
    token=os.getenv("SONARR_TOKEN"), api_url=os.getenv("SONARR_URL")
)


class TestSeries(unittest.TestCase):
    def test_should_raise(self):
        series = sonarr.find_series(term="Breaking Bad")
        with self.assertRaises(SeriesAlreadyExists):
            sonarr.request_series(series=series[0])


if __name__ == '__main__':
    unittest.main()
